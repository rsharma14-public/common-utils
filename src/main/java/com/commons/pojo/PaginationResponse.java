package com.commons.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class PaginationResponse {

	Object payload;
	long totalCount;
	long currentPage;
	int perPage;
//	long previousPage;
//	long nextPage;

}
