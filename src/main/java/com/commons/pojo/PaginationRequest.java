package com.commons.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class PaginationRequest<T> {

	T payload;
	@Setter(value=AccessLevel.NONE)
	int pageNo;
	Object sortBy;
	
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo<=0?1:pageNo;
	}
	
	

}
