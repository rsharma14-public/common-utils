package com.commons.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {

	private String token;
	private String sessionId;
	private String userId;
	private String username;
	private String fullname;
	private String contact;
	private String email;
	
	public UserDetails(String token, String sessionId, String userId) {
		super();
		this.token = token;
		this.sessionId = sessionId;
		this.userId = userId;
	}




}
