package com.commons.pojo;

import java.util.Map;

import lombok.Data;

@Data
public class GitPushPojo {

//	@Value("${git.remoreURL}")
	private String remoteUrl;
//	@Value("${git.username}")
	private String username;
//	@Value("${git.password}")
	private String token;

	private Map<String, Map<String, byte[]>> fileList;

	public GitPushPojo() {
	}

	public GitPushPojo(String remoteUrl, String username, String token, Map<String, Map<String, byte[]>> fileList) {
		super();
		this.remoteUrl = remoteUrl;
		this.username = username;
		this.token = token;
		this.fileList = fileList;
	}

	public GitPushPojo(Map<String, Map<String, byte[]>> fileList) {
		super();
		this.fileList = fileList;
	}

	/*
	 * public String getRemoteUrl() { return remoteUrl; }
	 * 
	 * public void setRemoteUrl(String remoteUrl) { this.remoteUrl = remoteUrl ==
	 * null ? this.remoteUrl : remoteUrl; }
	 * 
	 * public String getUsername() { return username; }
	 * 
	 * public void setUsername(String username) { this.username = username == null ?
	 * this.username : username; }
	 * 
	 * public String getToken() { return token; }
	 * 
	 * public void setToken(String token) { this.token = token == null ? this.token
	 * : token; }
	 * 
	 * public Map<String, Map<String, byte[]>> getFileList() { return fileList; }
	 * 
	 * public void setFileList(Map<String, Map<String, byte[]>> fileList) {
	 * this.fileList = fileList; }
	 */
}
