package com.commons.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomException extends Exception{

    private static long serialVersionUID = 1L;
    private String className;
    private String methodName;
    private String reasonCode;
    private String message;
    private String messageDetail;
    private String statusCode;
    private String actualExceptionTrace;
    private Exception excp;

}
