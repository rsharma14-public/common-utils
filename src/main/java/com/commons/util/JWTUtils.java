package com.commons.util;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTUtils {

	private static final String SIGN_ALGO = "RSA";
	// generated from getRSAKeys()
	private static final String PRIVATE_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCqoJnl+i/KyiaJjKGKTSpmL77QeIgY6magiFfd7Rq/1TlRbp7Y0mn1nGU3YKnHxLBhNVj3Vyxol9wct5SPydHuSE9z6bTEQGbFK4c3PvCcjsio7Cr01C/lLErUQN2cGSyQlbgTK4QLFGZaCrP6v3R5k55JdU4JyqE13W74vJ4ic+3/6KI31Y1y1mrqp1kj0aCLNpyvxA78lLYFpCuNI/LlnyU3wjt7G7OKsOXAeZlBiOFM8z8N9YYIfhPw95gP5KURAU96iiVXljiN+syWGHh+hIN80URqUzzvbVbAfKvAzUaGfb/MWZYG8shFYCq/uJ6rAvV8j4ISKdtPBOd0eLFlAgMBAAECggEARzAwZXonnHUQ9Qx+jWQ6j13WP8Y6CiMp5EpeVpJJOcYtXuNHVPEdEkNJG3yx+U3/xN7sGm39cjoBtN2HFyK9+quisprMN6Ei/5p0lMEmxDryT0ARgApTXUtIgRv7GdiK9lvw69aZBcJiHakfhDuCzgKf5MgTYhRvhDi5xv9y/q4tNXgzvJMp1pt9l4NMZJtxnDX76A6IuNXldGiJ2kwvlLA165+8G9s1E/eFK4Mfdd6chloAzxWHibgdWcJ6k346AML/EFaIpBnA9kfNlErdZD19SzrGUYdW2ISmkPcd6+uh+MiCFpU2omu3XGy0eNdHKm35XJz8ym9LjW2AISloAQKBgQDzI0C022W8+JzzCnsLfxGWYpMcPoFQm0k3z6feZBnrVNiI1JWzL+W6ZeAoTbUDVyQ8RgCK5xjJyN+LOmO+yqqQ8W1tIKT3VOexXxIwTxff8vWv3UdfGkJQxe0N17HRc4QApcenLpBkrSeUC2NR53HJueDswPKeBMJMP+r9Da7jZQKBgQCzp1xLbbr2sCi1G07S6S64vGbxlp9cKdzj/PyIdG90gl5rN4UHlztR4V8T1hB1vJJ63jKEbhunuTa/5a6c8qRIW2nneCY9f+IiQTSKv/GI1XUj5BJjGuPbafK9uFNSfQ7e1Gnu3jAhw/r+En7wx50bicmzUn8O2DcSAZAACqC2AQKBgCgttS2t69wUgWswWjEGKJPfakwKkDgP0UOlzynTBO18seO6rZpU2fO5jhda8ATDS7uiHcjWc6Q0y6woU/9dYl5dZyXXdStiHn+TJGBTUbCyZ6NOP9MZdcf/TTMyZ7IjErdArgOUkEvyyU2PrDQGuSkKzH6TTASM6MbM8TosHrB5AoGAHdX5mf1LQ5TV7d3vJU/clomTfyPIq4G4UZS+S9sqd2r/S0cZLL2M+Hg5Vg4DBcqfeRBVy3CN3zj/QrdeXGpfpjSoTezCTQxj/dReaXyfHF526dMCyrq4hMP/sEpgbB6gzZjxTt5PmAv4LyHbOxmrmds0SDe3Kjv2SWNBCwC42AECgYAsiTDJzZsNS4KcM2ylrqRru048YOfgxGANTM7+nI8kbhTILiUsqQpTk5ePZc2kablBo4e0LTOqsRzEDkP6lA2rbHMyJ415d0ScMhr7ckDAmDONBKqIRZRn9RhXzmV2Tb7Qz5rDefrkNeGBPYpqvtpVPdXwNyeirh2Y6J2TspP7aQ==";
	private static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqqCZ5fovysomiYyhik0qZi++0HiIGOpmoIhX3e0av9U5UW6e2NJp9ZxlN2Cpx8SwYTVY91csaJfcHLeUj8nR7khPc+m0xEBmxSuHNz7wnI7IqOwq9NQv5SxK1EDdnBkskJW4EyuECxRmWgqz+r90eZOeSXVOCcqhNd1u+LyeInPt/+iiN9WNctZq6qdZI9Ggizacr8QO/JS2BaQrjSPy5Z8lN8I7exuzirDlwHmZQYjhTPM/DfWGCH4T8PeYD+SlEQFPeoolV5Y4jfrMlhh4foSDfNFEalM8721WwHyrwM1Ghn2/zFmWBvLIRWAqv7ieqwL1fI+CEinbTwTndHixZQIDAQAB";

	private static final String className = "JWTUtils";

	public static String generateJWTToken(String name,Object claims, int tokenValidity) {

		try {

			// long nowMillis = System.currentTimeMillis();
			Date now = new Date();
			PrivateKey signingKey = KeyFactory.getInstance(SIGN_ALGO)
					.generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(PRIVATE_KEY)));

			// Let's set the JWT Claims
			Map<String, Object> claimData = new HashMap<>();
			claimData.put("data", claims);
			JwtBuilder builder = Jwts.builder()
					.setClaims(claimData)
					.setId(Utils.getUUID())
					.setIssuedAt(now)
					.setIssuer("HMS")
					.setSubject(name)
					.setExpiration(Utils.addDateTime(now, "SECOND", tokenValidity))
					.signWith(SignatureAlgorithm.RS256, signingKey);

			return builder.compact();

		} catch (Exception e) {
			LogUtils.logError(className, "generateJWTToken", "Unable to generate JWT token.Reason=" + e.getMessage());
		}
		return null;
	}

	public static boolean validateJWTToken(String token) throws Exception {
		try {

			PublicKey signingKey = KeyFactory.getInstance(SIGN_ALGO)
					.generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(PUBLIC_KEY)));

			Claims claims = (Claims) Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token).getBody();
			LogUtils.logInfo(className, "validateJWTToken", "claims=" + claims);

			return true;
		} catch (Exception e) {
			LogUtils.logError(className, "validateJWTToken", "Unable to validate JWT token.Reason=" + e.getMessage());
			throw new Exception(e);
		}
	}

	private static Map<String, Object> getRSAKeys() throws Exception {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();
		Map<String, Object> keys = new HashMap<String, Object>();
		System.out.println("privateKey=" + Base64.getEncoder().encodeToString(privateKey.getEncoded()));
		System.out.println("publicKey=" + Base64.getEncoder().encodeToString(publicKey.getEncoded()));

		keys.put("private", privateKey);
		keys.put("public", publicKey);
		return keys;
	}

	public static String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	public static  Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public static  <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	private static  Claims getAllClaimsFromToken(String token) {
		try {
			PublicKey signingKey = KeyFactory.getInstance(SIGN_ALGO)
					.generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(PUBLIC_KEY)));
			return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token).getBody();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// check if the token has expired
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

//	public static void main(String[] args) {
//		new JWTUtils().generateJWTToken(null);
//	}
}
