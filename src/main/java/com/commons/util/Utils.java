package com.commons.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.web.util.UriComponentsBuilder;

import com.commons.pojo.GitPushPojo;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Utils {

	private static final String className="Utils";
	
	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	public static String clearNull(Object obj) {
		return obj == null ? "" : obj.toString();
	}

	public static int getInt(Object obj) {
		try {
			return Integer.parseInt(obj+"");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static double getDouble(Object obj) {
		try {
			return Double.parseDouble(obj+"");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.0;
	}
	
	public static Map<String, Object> getRequestParams(HttpServletRequest request) {
		Enumeration<String> parameterNames = request.getParameterNames();
		Map<String, Object> objReq = new HashMap<>();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement().trim();

			String[] paramValues = request.getParameterValues(paramName);
			objReq.put(paramName, request.getParameter(paramName).trim());

		}
		return objReq;
	}

	public static Object mapEntityToDTO(Object entityObject, Object dtoObject) {
		Class entityCls = entityObject.getClass();
		Class dtoCls = dtoObject.getClass();

		List<Method> dtoSetter = new ArrayList<Method>();
		List<Method> entityGetter = new ArrayList<Method>();
		
		Method[] dtoMethods = dtoCls.getDeclaredMethods();
		Method[] entityMethods = entityCls.getDeclaredMethods();

		for (Method method : dtoMethods) {
			if (isSetter(method)) {
				dtoSetter.add(method);
			}

		}
		
		for (Method method : entityMethods) {
			if (isGetter(method)) {
				entityGetter.add(method);
			}

		}


		Method m1=null;
		Method m2=null;
		
		for(int i=0;i<entityGetter.size();i++) {
			for(int j=0;j<dtoSetter.size();j++) {
				//System.out.println(entityGetter.get(i).getName().substring(3)+"="+dtoSetter.get(j).getName().substring(3));

				if(entityGetter.get(i).getName().substring(3).equals(dtoSetter.get(j).getName().substring(3))) {
					//System.out.println(entityGetter.get(i).getName().substring(3)+"=="+dtoSetter.get(j).getName().substring(3));

					m1 = entityGetter.get(i);
					m2=dtoSetter.get(j);
					try {
						m2.invoke(dtoObject, m1.invoke(entityObject));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						System.out.println(e.getMessage()+":"+m1.getName()+"==>"+m2.getName());
					}
					break;
				}
			}
		}

		return dtoObject;
	}

	public static Object mapDTOToEntity(Object dtoObject, Object entityObj) {
		Class entityCls = entityObj.getClass();
		Class dtoCls = dtoObject.getClass();

		List<Method> dtoGetter = new ArrayList<Method>();
		List<Method> entitySetter = new ArrayList<Method>();
		
		Method[] dtoMethods = dtoCls.getDeclaredMethods();
		Method[] entityMethods = entityCls.getDeclaredMethods();

		for (Method method : dtoMethods) {
			if (isGetter(method)) {
				dtoGetter.add(method);
			}

		}
		
		for (Method method : entityMethods) {
			if (isSetter(method)) {
				entitySetter.add(method);
			}

		}


		Method m1=null;
		Method m2=null;
		
		for(int i=0;i<dtoGetter.size();i++) {
			for(int j=0;j<entitySetter.size();j++) {
				//System.out.println(dtoGetter.get(i).getName().substring(3)+"="+entitySetter.get(j).getName().substring(3));

				if(dtoGetter.get(i).getName().substring(3).equals(entitySetter.get(j).getName().substring(3))) {
					//System.out.println(dtoGetter.get(i).getName().substring(3)+"=="+entitySetter.get(j).getName().substring(3));

					m1 = dtoGetter.get(i);
					m2=entitySetter.get(j);
					try {
						m2.invoke(entityObj, m1.invoke(dtoObject));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						System.out.println(e.getMessage());
					}
					break;
				}
			}
		}

		return entityObj;
	}

	public static Object mapFromObjToObj(Object fromObj, Object toObj) {
		Class toCls = toObj.getClass();
		Class fromCls = fromObj.getClass();

		List<Method> fromGetter = new ArrayList<Method>();
		List<Method> toSetter = new ArrayList<Method>();
		
		Method[] fromMethods = fromCls.getDeclaredMethods();
		Method[] entityMethods = toCls.getDeclaredMethods();

		for (Method method : fromMethods) {
			if (isGetter(method)) {
				fromGetter.add(method);
			}

		}
		
		for (Method method : entityMethods) {
			if (isSetter(method)) {
				toSetter.add(method);
			}

		}


		Method m1=null;
		Method m2=null;
		
		for(int i=0;i<fromGetter.size();i++) {
			for(int j=0;j<toSetter.size();j++) {
				//System.out.println(dtoGetter.get(i).getName().substring(3)+"="+entitySetter.get(j).getName().substring(3));

				if(fromGetter.get(i).getName().substring(3).equals(toSetter.get(j).getName().substring(3))) {
					//System.out.println(fromGetter.get(i).getName().substring(3)+"=="+toSetter.get(j).getName().substring(3));

					m1 = fromGetter.get(i);
					m2=toSetter.get(j);
					try {
						m2.invoke(toObj, m1.invoke(fromObj));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						System.out.println(e.getMessage());
					}
					break;
				}
			}
		}

		return toObj;
	}
	
	public static boolean isGetter(Method method) {
		if (Modifier.isPublic(method.getModifiers()) && method.getParameterTypes().length == 0) {
			if (method.getName().matches("^get[A-Z].*") && !method.getReturnType().equals(void.class))
				return true;
		}
		return false;
	}

	public static boolean isSetter(Method method) {
		return Modifier.isPublic(method.getModifiers()) && method.getReturnType().equals(void.class)
				&& method.getParameterTypes().length == 1 && method.getName().matches("^set[A-Z].*");
	}
	
	public static String getFormInput(String type,String name,String label,String placeholder,
			Object value,boolean required,Map<String,String> inputs) {
		if(inputs==null)
			inputs=new HashMap<>();
		
		WeakReference<String> labelClass= new WeakReference<String>(Utils.clearNull(inputs.get("labelClass")));
		WeakReference<String> inputClass= new WeakReference<String>(Utils.clearNull(inputs.get("inputClass")));
		WeakReference<String> inputDivClass= new WeakReference<String>(Utils.clearNull(inputs.get("inputDivClass")));

		String formInput="<div class='form-group'>" + 
				"			<label class='"+(!labelClass.get().isEmpty()?labelClass.get():"col-md-4 control-label")+"' for='"+name+"'>"+label+"</label>" + 
				"			<div class='"+(!inputDivClass.get().isEmpty()?inputDivClass.get():"col-md-8")+"'>" + 
				"				<input type='"+type+"' path='"+name+"' id='"+name+"' class='"+(inputClass.get().length()>0?inputClass:"form-control")+"' name='"+name + 
				"'					value='"+Utils.clearNull(value)+"'	placeholder='"+(placeholder!=null?placeholder:"Input")+"' required="+(required?required:false)+">" + 
				"<div class='has-error'>"+
                "<div id='error_"+name+"' class='error'>"+
				"</div></div></div></div>";
		return formInput;
	}
	
	public static String getFormRadio(String name,String label,boolean required,Map<String,String> inputs,Map<String,Object> radio) {
		if(inputs==null)
			inputs=new HashMap<>();
		
		WeakReference<String> labelClass= new WeakReference<String>(Utils.clearNull(inputs.get("labelClass")));
		WeakReference<String> inputClass= new WeakReference<String>(Utils.clearNull(inputs.get("inputClass")));
		WeakReference<String> inputDivClass= new WeakReference<String>(Utils.clearNull(inputs.get("inputDivClass")));
		
		String formInput="<div class='form-group'>" + 
				"<label class='"+(labelClass.get().length()>0?labelClass.get():"col-md-4 control-label")+"' for='"+name+"'>"+label+"</label>" + 
				"<div class='"+(!inputDivClass.get().isEmpty()?inputDivClass.get():"col-md-8")+"'>" ;
				for(String k:radio.keySet()) {
					formInput+="<input type='radio' path='"+name+"' value='"+radio.get(k)+"' class='form-control' />"+k; 
				}
				formInput+="<div class='has-error'>" + 
						"  <form:errors path='"+name+"' class='help-inline'/>" + 
						"   </div></div></div>";
		
		return formInput;
	}
	
	public static Map<String,String> getCombobox(ArrayList<Object> obj, String getValFunction,String delimit1, String getDisplayFunction, String delimit2,boolean encodeValue,boolean normalWithEncodeValue,boolean defaultOp)
	{
		Map<String,String> list=new LinkedHashMap<>();
		
		try
		{
			if(defaultOp) {
				list.put("", "Select");
			}
			for (Iterator<Object> iterator = obj.iterator(); iterator.hasNext();)
			{
				Object object = iterator.next();
				String[] methodCalls1 = getValFunction.split(",");
				String[] methodCalls2 = getDisplayFunction.split(",");
				String nameValue1 = "";
				String nameValue2 = "";
				
				for (int i = 0; i < methodCalls1.length; i++)
				{
					Method methodDisplay = object.getClass().getMethod(methodCalls1[i]);
					Object returnVal = methodDisplay.invoke(object, null);
					if (returnVal!= null && !returnVal.toString().trim().isEmpty())
						nameValue1 +=  clearNull(returnVal) + clearNull(delimit1);
				}
				nameValue1 = delimit1.isEmpty()?nameValue1:nameValue1.substring(0, nameValue1.length() - 1);

				for (int i = 0; i < methodCalls2.length; i++)
				{
					Method methodDisplay = object.getClass().getMethod(methodCalls2[i]);
					Object returnVal = methodDisplay.invoke(object, null);
					
					if (returnVal!= null && !returnVal.toString().trim().isEmpty())
						nameValue2 += clearNull(returnVal) + clearNull(delimit2);
				}
				nameValue2 = delimit2.isEmpty()?nameValue2:nameValue2.substring(0, nameValue2.length() - 1);

				if(encodeValue)
					nameValue1=DecryptionEncryptionUtil.encodingText(nameValue1);
				else if(normalWithEncodeValue &&  encodeValue)
					nameValue1=DecryptionEncryptionUtil.encodingText(nameValue1);
				
				list.put(nameValue1, nameValue2);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			list=new HashMap<>();
		}
		return list;
	}

	public static boolean isNullOrEmpty(Object object) {
		if(object ==null || object.toString().trim().isEmpty())
			return true;
		
		return false;
	}

	public static boolean isNullOrEmptyorNot(Object object, String extra) {
		if(object ==null || object.toString().trim().isEmpty() || object.toString().equals(extra))
			return true;
		
		return false;
	}

	public static String formatDateToString(Date date, String pattern) {

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			if (date == null)
				date = new Date();
			return sdf.format(date);
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static Date formatStringToDate(String dateString, String pattern) {

		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			return simpleDateFormat.parse(dateString);
		} catch (Exception e) {
			return new Date();
		}
	}
	public static String getURI(String url, Map<String, Object> req1, Map<String, Object> req2) {
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(url);
		if (req1 != null) {
			for (Map.Entry<String, Object> req : req1.entrySet()) {
				uri.queryParam(req.getKey(), req.getValue());
			}
		}
		if (req2 != null) {

			for (Map.Entry<String, Object> req : req2.entrySet()) {
				uri.queryParam(req.getKey(), req.getValue());
			}
		}

		return uri.toUriString();
	}
	
	public static Date addDateTime(Date dateToAdd, String type, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone. getTimeZone("IST"));
		cal.setTime(dateToAdd);
		switch (type) {
		case "SECOND":
			cal.add(Calendar.SECOND, amount);
			break;
		case "MINUTE":
			cal.add(Calendar.MINUTE, amount);
			break;
		case "HOUR":
			cal.add(Calendar.HOUR, amount);
			break;
		case "DATE":
			cal.add(Calendar.DATE, amount);
			break;
		default:
			break;
		}
		return cal.getTime();
	}
 
	public static Date reduceDateTime(Date dateToAdd, String type, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateToAdd);
		switch (type) {
		case "SECOND":
			cal.add(Calendar.SECOND, amount);
			break;
		case "MINUTE":
			cal.add(Calendar.MINUTE, amount);
			break;
		case "HOUR":
			cal.add(Calendar.HOUR, amount);
			break;
		case "DATE":
			cal.add(Calendar.DATE, amount);
			break;
		default:
			break;
		}
		return cal.getTime();
	}
	public static Date getDatesInRange(Date dateToAdd, int weekValue,int dayValue) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateToAdd);
//		cal.set(Calendar.MONTH, Calendar.MARCH);
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		cal.set(Calendar.WEEK_OF_MONTH,weekValue);
		cal.set(Calendar.DAY_OF_WEEK, dayValue);
		
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	    
		
		return cal.getTime();
	}
	public static Date getOnlyDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
//		cal.setTimeZone(TimeZone.getTimeZone("IST"));
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	public static String convertObjectToJson(Object obj){
		
		try {
			return new ObjectMapper()
			 .setVisibility(PropertyAccessor.FIELD, Visibility.ANY)
			 .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
			 //.writerWithDefaultPrettyPrinter()
			.writeValueAsString(obj);
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
        
		return null;
	}
	
	private static <T> T getType(Object obj, Class<T> valueType) throws IOException {

		return new ObjectMapper()
		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		 .readValue(obj.toString(), valueType);

	}

	public static <T> T getObjectType(Object requestType, Class<T> valueType) throws Exception {

		T response = null;
		try {
			response = getType(requestType, valueType);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}
	
	public static RevCommit pushToRemote(GitPushPojo gitReq) throws Exception {
		LogUtils.logInfo(className, "pushToRemote", "started");

		// prepare a new folder for the cloned repository
		File localPath = File.createTempFile("GitRepository", "");
		if (!localPath.delete()) {
			throw new IOException("Could not delete temporary file " + localPath);
		}

		// then clone
		//TBD: need to use 1st checkout repo and do pull rebase sothat no new repo everytime
		LogUtils.logInfo(className, "pushToRemote", "Cloning from " + gitReq.getRemoteUrl() + " to " + localPath);
		try (Git git = Git.cloneRepository().setURI(gitReq.getRemoteUrl())
				.setCredentialsProvider(new UsernamePasswordCredentialsProvider(gitReq.getUsername(), gitReq.getToken()))
				.setDirectory(localPath).call()) {

			Repository repository = git.getRepository();
			File theDir = null;
			File file = null;
			for (Map.Entry<String, Map<String, byte[]>> dir : gitReq.getFileList().entrySet()) {
				// create the folder
				theDir = new File(repository.getDirectory().getParent(), dir.getKey());
				if (!theDir.exists())
					theDir.mkdir();

				for (Map.Entry<String, byte[]> fl : dir.getValue().entrySet()) {
					// create the file
					file = new File(theDir, fl.getKey());
					// myfile.createNewFile();
					try (OutputStream os = new FileOutputStream(file)) {
						os.write(fl.getValue());
					}
				}
			}
			// Stage all files in the repo including new files
			git.add().addFilepattern(".").call();

			// and then commit the changes.
			git.commit().setMessage("New files added").call();

			// Stage all changed files, omitting new files, and commit with one command
			git.commit().setAll(true).setMessage("Commit changes to existing files").call();
			git.add().addFilepattern("*").call();
			RevCommit result = git.commit().setMessage("initial commit").call();
			git.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(gitReq.getUsername(), gitReq.getToken()))
					.call();
			LogUtils.logInfo(className, "pushToRemote", "ended");
			return result;
		}
	}

}
