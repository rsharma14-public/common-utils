package com.commons.util;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;

@Component
public class RestTemplateUtil {
	static RestTemplate restTemplate;
	static {
		restTemplate=new RestTemplate();
	}
	@Autowired
	private ObjectMapper mapper;
	
	public ResponseEntity<?> restExchange(String url, HttpMethod httpMethod, HttpEntity<?> httpEntity,
			ParameterizedTypeReference<?> typeRef) throws Exception{
		LogUtils.logInfo(this.getClass().getSimpleName(),"restExchange", "Rest call::"+"\nmethod="+httpMethod.toString()+"\nurl="+url+"\nrequest= "+(httpEntity!=null?mapper.writeValueAsString(httpEntity.getBody())+"\nheader="+httpEntity.getHeaders().toString():""));
		Stopwatch stopwatch = Stopwatch.createStarted();
		ResponseEntity<?> resp=restTemplate.exchange(url, httpMethod,httpEntity, typeRef);
		stopwatch.stop();
		LogUtils.logInfo(this.getClass().getSimpleName(),"restExchange", "Rest call end::"+"\nmethod="+httpMethod.toString()+"\nurl="+url+"\nresponse= "+mapper.writeValueAsString(resp.getBody())+"\nStatus="+resp.getStatusCodeValue()+"\nheader="+resp.getHeaders().toString()+"\nElapsedTime="+stopwatch.elapsed(TimeUnit.MILLISECONDS));


		return resp;
	}

	public ResponseEntity<?> restExchange(String url, HttpMethod httpMethod, HttpEntity<?> httpEntity,
			Class<?> class1) throws Exception{
		
		LogUtils.logInfo(this.getClass().getSimpleName(),"restExchange", "Rest call::"+"\nmethod="+httpMethod.toString()+"\nurl= "+url+"\nrequest= "+(httpEntity!=null?mapper.writeValueAsString(httpEntity.getBody())+"\nheader="+httpEntity.getHeaders().toString():""));
		Stopwatch stopwatch = Stopwatch.createStarted();
		ResponseEntity<?> resp=restTemplate.exchange(url, httpMethod,httpEntity, class1);
		stopwatch.stop();
		LogUtils.logInfo(this.getClass().getSimpleName(),"restExchange", "Rest call end::"+"\nmethod="+httpMethod.toString()+"\nurl="+url+"\nresponse= "+mapper.writeValueAsString(resp.getBody())+"\nStatus="+resp.getStatusCodeValue()+"\nheader="+resp.getHeaders().toString()+"\nElapsedTime="+stopwatch.elapsed(TimeUnit.MILLISECONDS));

		return resp;

	}

	public ResponseEntity<?> restPost(String url, HttpEntity<?> httpEntity,Class typeRef) throws Exception{
		LogUtils.logInfo(this.getClass().getSimpleName(),"restPost", "Rest call::"+"\nmethod=POST \nurl="+url+"\nrequest= "+(httpEntity!=null?mapper.writeValueAsString(httpEntity.getBody())+"\nheader="+httpEntity.getHeaders().toString():""));
		Stopwatch stopwatch = Stopwatch.createStarted();
		ResponseEntity<?> resp=restTemplate.postForEntity(url, httpEntity, typeRef);
		stopwatch.stop();
		LogUtils.logInfo(this.getClass().getSimpleName(),"restPost", "Rest call end::"+"\nmethod=POST\nurl= "+url+"\nresponse= "+mapper.writeValueAsString(resp.getBody())+"\nStatus="+resp.getStatusCodeValue()+"\nheader="+resp.getHeaders().toString()+"\nElapsedTime="+stopwatch.elapsed(TimeUnit.MILLISECONDS));


		return resp;
	}
	
	public ResponseEntity<?> restGet(String url,Class typeRef) throws Exception{
		LogUtils.logInfo(this.getClass().getSimpleName(),"restPost", "Rest call::"+"\nmethod=GET\nurl= "+url);
		Stopwatch stopwatch = Stopwatch.createStarted();
		ResponseEntity<?> resp=restTemplate.getForEntity(url, typeRef);
		stopwatch.stop();
		LogUtils.logInfo(this.getClass().getSimpleName(),"restPost", "Rest call end::"+"\nmethod=POST\nurl="+url+"\nresponse= "+mapper.writeValueAsString(resp.getBody())+"\nStatus="+resp.getStatusCodeValue()+"\nheader="+resp.getHeaders().toString()+"\nElapsedTime="+stopwatch.elapsed(TimeUnit.MILLISECONDS));


		return resp;
	}
	
}
