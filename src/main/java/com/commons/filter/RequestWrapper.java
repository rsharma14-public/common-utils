package com.commons.filter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.tomcat.util.http.fileupload.IOUtils;

public class RequestWrapper extends HttpServletRequestWrapper
{
    public class CachedServletInputStream extends ServletInputStream
    {

        private ByteArrayInputStream input;

        public int read()
            throws IOException
        {
            return input.read();
        }

        public boolean isFinished()
        {
            return input.available() == 0;
        }

        public boolean isReady()
        {
            return true;
        }

        public void setReadListener(ReadListener arg0)
        {
            throw new RuntimeException("Not implemented");
        }

        public CachedServletInputStream()
        {
            super();
            input = new ByteArrayInputStream(cachedBytes.toByteArray());
        }
    }


    private ByteArrayOutputStream cachedBytes;

    public RequestWrapper(HttpServletRequest request)
    {
        super(request);
    }

    public ServletInputStream getInputStream()
        throws IOException
    {
        if(cachedBytes == null)
        {
            cacheInputStream();
        }
        return new CachedServletInputStream();
    }

    public BufferedReader getReader()
        throws IOException
    {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    private void cacheInputStream()
        throws IOException
    {
        cachedBytes = new ByteArrayOutputStream();
        IOUtils.copy(super.getInputStream(), cachedBytes);
    }

}
